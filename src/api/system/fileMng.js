const baseUrl = process.env.BASE_API
import request from '@/utils/request'

export function displayImageUrl(filePath) {
  return baseUrl + '/admin-service/api/fileMng/displayImage?filePath=' + filePath
}

export function fileUploadUrl() {
  return baseUrl + '/admin-service/api/fileMng/upload'
}

export function fileDirectUploadUrl() {
  return baseUrl + '/admin-service/api/fileMng/directUpload'
}

export function downloadAttachment(filePath, fileName) {
  return request({
    url: 'admin-service/api/fileMng/downloadAttachment',
    method: 'get',
    params: {
      filePath
    },
    fileName: fileName,
    responseType: 'blob' // 表明返回服务器返回的数据类型
  })
}

export function downloadSysAttachment(filePath, fileName) {
  return request({
    url: 'admin-service/api/fileMng/downloadSysAttachment',
    method: 'get',
    params: {
      filePath
    },
    fileName: fileName,
    responseType: 'blob' // 表明返回服务器返回的数据类型
  })
}
